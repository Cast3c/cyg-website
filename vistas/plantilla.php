<!doctype html>
<html lang="en">

<head>
   <?php 
        include"./vistas/inc/link.php"
   ?>
    <title><?php echo COMPANY ?></title>
</head>

<body>
    <!-- Encabezado-->
    <?php 
        include"./vistas/inc/hero.php"
    ?>

    <!--- Contenido --->
    <?php
        include"./vistas/inc/content.php"
    ?>

    <!--- footer --->
    <?php 
        include"./vistas/inc/footer.php"
    ?>
    
    <!--- scripts --->
    <?php
        include"./vistas/inc/script.php"
    ?>
</body>

</html>