

$(document).ready(function(){
    $('#lightSlider').lightSlider({ 
        autoWidth: true,
        loop: true,
        rtl: true,
        controls: false,
        onSliderLoad: function(){
            $('#lightSlider').removeClass('cS-hidden'); 
        }
    });
});

