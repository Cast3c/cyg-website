 <!-- Required meta tags -->
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="<?php echo SERVERURL; ?>/vistas/style.css">
 <link rel="stylesheet" href="<?php echo SERVERURL; ?>/vistas/flaticon.css">
 <link type="text/css" rel="stylesheet" href="<?php echo SERVERURL; ?>/vistas/lightSlider/css/lightslider.css">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
 <script src="<?php echo SERVERURL; ?>/vistas/lightSlider/js/lightslider.js"></script>
 <script src="<?php echo SERVERURL; ?>/vistas/config.js"></script>
 <link rel="shortcut icon" href="<?php echo SERVERURL; ?>/vistas/img/IMG-3521.PNG" type="image/x-icon">

 <!-- Bootstrap CSS -->
 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">