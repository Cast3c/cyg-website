<div class="content">
    <div id="products">
        <span class="big-box"></span>
        <img src="<?php echo SERVERURL; ?>/vistas/img/carreta-cajas.png" class="box" alt="">
        <div class="products-header">
            <h1>
                Servicios
            </h1>
        </div>
        <div class="products-content">
            <div class="container">
                <div class="row">
                    <div class="col-4">
                        <div id="service-body1">
                            <div id="service-title">
                                <h1>
                                    Mudanzas
                                </h1>
                            </div>
                            <div id="service-icon">
                                <span>
                                    <i class="flaticon-delivery"></i>
                                </span>
                            </div>
                            <div id="service-caption">
                                <span>
                                    <h3>
                                        En CyG entendemos las preocupaciones de las mudanzas, cotiza con nosotros la
                                        tuya
                                    </h3>
                                </span>
                            </div>
                            <div id="service-action">
                                <span>
                                    <a href="https://wa.link/wp8rdo" class="service-btn">
                                        Cotizar
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div id="service-body2">
                            <div id="service-title">
                                <h1>
                                    Encomiendas
                                </h1>
                            </div>
                            <div id="service-icon">
                                <span>
                                    <i class="flaticon-drop-shipping"></i>
                                </span>
                            </div>
                            <div id="service-caption">
                                <span>
                                    <h3>
                                        Envia y recibe documentos, paquetes y mercancias de diferenetes tamaños en
                                        Colombia
                                    </h3>
                                </span>
                            </div>
                            <div id="service-action">
                                <span>
                                    <a href="https://wa.link/203lzz" class="service-btn">
                                        Cotizar
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div id="service-body3">
                            <div id="service-title">
                                <h1>
                                    Carga especial
                                </h1>
                            </div>
                            <div id="service-icon">
                                <span>
                                    <i class="flaticon-boxes"></i>
                                </span>
                            </div>
                            <div id="service-caption">
                                <span>
                                    <h3>
                                        Vehículos, personal y logística, para entregar tu mercancía a tiempo y en
                                        las
                                        condiciones requeridas
                                    </h3>
                                </span>
                            </div>
                            <div id="service-action">
                                <span>
                                    <a href="https://wa.link/bvqyuu" class="service-btn">
                                        Cotizar
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-products">
            <div class="mobile-title">
                Servicios
            </div>
            <div class="slider-box">
                <ul id="lightSlider" class="">
                    <li>
                        <div class="slider-details1">
                            <h1>Mudanzas</h1>
                            <span id="slider-icon">
                                <i class="flaticon-delivery"></i>
                            </span>
                            <p>En CyG entendemos las preocupaciones de una mudanza.
                                Contactanos y cotiza con nosotros tu mudanza</p>
                            <a href="https://wa.link/wp8rdo">Cotizar</a>
                        </div>
                    </li>
                    <li>
                        <div class="slider-details2">
                            <h1>Encomiendas</h1>
                            <span id="slider-icon">
                                <i class="flaticon-delivery"></i>
                            </span>
                            <p>Envia y recibe documentos, paquetes y mercancias de diferenetes tamaños en
                                Colombia</p>
                            <a href="https://wa.link/203lzz">Cotizar</a>
                        </div>
                    </li>
                    <li>
                        <div class="slider-details3">
                            <h1>Carga especializada</h1>
                            <span id="slider-icon">
                                <i class="flaticon-delivery"></i>
                            </span>
                            <p>Vehículos, personal y logística, para entregar tu mercancía a tiempo y en las
                                condiciones requeridas</p>
                            <a href="https://wa.link/bvqyuu">Cotizar</a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="contact">
        <div class="container">
            <span class="big-circle"></span>
            <img src="<?php echo SERVERURL; ?>/vistas/img/box.png" class="square" alt="">
            <div class="form">
                <div class="contact-info">
                    <h3 class="title">Encuentranos en</h3>
                    <p class="text">

                    </p>
                    <div class="info">
                        <div class="information">
                            <img src="<?php echo SERVERURL; ?>/vistas/img/map.png" alt="" class="icon">
                            <p>Cra 16c # 162-24 Bogota - Colombia </p>
                        </div>
                        <div class="information">
                            <img src="<?php echo SERVERURL; ?>/vistas/img/email.png" alt="" class="icon">
                            <p>cygtransportesco@gmail.com</p>
                        </div>
                        <div class="information">
                            <img src="<?php echo SERVERURL; ?>/vistas/img/phone-call.png" alt="" class="icon">
                            <p>+57 313-212-9686 </p>
                        </div>
                    </div>
                    <div class="social-media">
                        <p>Conectate con nosotros en:</p>
                        <div class="social-icon">
                            <a href="https://www.facebook.com/CyG-Log%C3%ADstica-y-Transporte-103886387832067" class="contact-container">
                                <i class="flaticon-facebook-circular-logo"></i>
                            </a>
                            <a href="https://www.instagram.com/cyg_logistica/" class="contact-container">
                                <i class="flaticon-instagram"></i>
                            </a>
                            <a href="https://wa.me/+573132129686?text=Hola%20quisiera%20mas%20información">
                                <i class="flaticon-whatsapp"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="contact-form">
                    <span class="circle one"></span>
                    <span class="circle two"></span>
                    <form method="post" action="mailto:cygtransportesco@gmail.com">
                        <h3 class="tiltle">Escribenos</h3>
                        <div class="input-container">
                            <input type="text" name="nombre" id="" class="input">
                            <label for="">Nombre</label>
                            <span>Nombre</span>
                        </div>
                        <div class="input-container">
                            <input type="text" name="email" id="" class="input">
                            <label for="">Correo electronico</label>
                            <span>Correo electronico</span>
                        </div>
                        <div class="input-container">
                            <input type="tel" name="telefono" id="" class="input">
                            <label for="">Telefono</label>
                            <span>Telefono</span>
                        </div>
                        <div class="input-container">
                            <textarea name="mensaje" id="" cols="10" rows="3" class="input"></textarea>
                            <label for="">Mensaje</label>
                            <span>Mensaje</span>
                        </div>
                        <input type="submit" value="Enviar" class="btn">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="about">
        <div class="container">
            <div class="cyg-text">
                <div class="logo-mb">
                </div>
                <div class="about-content">
                    <div class="partners-logo">
                    </div>
                    <div class="text-mb">
                        <h1>CyG Logistica y transporte</h1>
                        <p>
                            Nuestra prioridad es solucionar los desafios que se presentan en la cadena
                            logistica
                            de nuestros
                            clientes.
                            Buscamos siempre facilitar la labor de la logistica y el transporte de carga en
                            sus
                            varias
                            modalidades, entendemos
                            que los desafios actuales nos exigen estar a la vanguardia de las metodologias,
                            certificaciones
                            y tecnologias,
                            por tal razon siempre estaremos en la busqueda de la mejor opcion para nuestros
                            clientes y
                            colaboradores.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="nav-bottom">
        <div class="nav-bottom">
            <ul class="nav fixed-bottom justify-content-center mobile-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#hero">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#products">Servicios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contact">Contacto</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#about">Nosotros</a>
                </li>
            </ul>
        </div>
    </div>
</div>