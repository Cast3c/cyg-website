<footer>
    <div id="cyg_footer">
        <div class="logoyredes">
            <div class="logoCyg">
                <img src="<?php echo SERVERURL; ?>/vistas/img/IMG-3521.PNG" alt="">
            </div>
            <h1>Logistica y Transporte</h1>
            <div class="redesCyg">
                <img src="<?php echo SERVERURL; ?>/vistas/img/redes/instagram.png" alt="">
            </div>
            <div class="redesCyg">
                <img src="<?php echo SERVERURL; ?>/vistas/img/redes/facebook.png" alt="">
            </div>
            <div class="redesCyg">
                <img src="<?php echo SERVERURL; ?>/vistas/img/redes/whatsapp.png" alt="">
            </div>
        </div>
        <div class="row mb-opt">
            <div class="col col-mb1">
                <h1>Clientes</h1>
                <ul class="list-unstyled">
                    <li>Servicios</li>
                    <li>Contacto</li>
                    <li>PQRs</li>
                </ul>
            </div>
            <div class="col col-mb2">
                <h1>Aliados</h1>
                <ul class="list-unstyled">
                    <li>APP-CyG</li>
                    <li>Viajes</li>
                </ul>
            </div>
            <div class="col col-mb3">
                <h1>Empresa</h1>
                <ul class="list-unstyled">
                    <li>Docs</li>
                    <li>Info</li>
                    <li>Empleos</li>
                </ul>
            </div>
        </div>
    </div>
    <div id="cygmb_footer">
        <div class="mb_logoCyg">
            <img src="<?php echo SERVERURL; ?>/vistas/img/IMG-3521.PNG" alt="">
        </div>
        <div id="cygTitle">
            <h1>CyG Logistica y Transporte</h1>
        </div>
    </div>
    <div class="developer">
        <a href="">Desarrollado por: TecDevelop</a>
    </div>
</footer>