<div id="hero">
    <div class="container">
        <nav class="navbar sticky-top navbar-expand-lg navbar-light bg-light" id="nav-web">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">
                    <div class="container ">
                        <img class="logo" src="<?php echo SERVERURL; ?>/vistas/img/IMG-3521.PNG" alt="">
                    </div>
                </a>
                <button class="navbar-toggler btn-danger" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse items-nav-web" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#landing-section" id="nav-web-item">Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#products" id="nav-web-item">Servicios</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link " href="#about" id="nav-web-item">Quienes somos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#contact" id="nav-web-item">Contacto</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <div class="container" id="landing-info">
        <div class="row" id="landind-right">
            <div class="col-6">
                <div class="landing-left">
                    <div id="subtitle5">
                        <h3>
                            Recogemos, cargamos y entregamos
                        </h3>
                    </div>
                    <div id="title1">
                        <h1>
                            ¡Facil!
                        </h1>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="landing-right">
                    <a href="https://wa.link/ay9kmq" class="landing-btn">
                        <i class="flaticon-whatsapp"></i>
                        Contactanos
                    </a>
                </div>
            </div>
        </div>

    </div>
    <div class="container" id="mobile-landing">
        <div class="row">
            <div id="mobile-subtitle">
                <h3>
                    Recogemos, cargamos y entregamos
                </h3>
            </div>
            <div id="mobile-title">
                <h1>
                    ¡Facil!
                </h1>
            </div>
            <div id="mobile-button">
                <a href="">
                    <i class="flaticon-whatsapp"></i>
                    Contactanos
                </a>
            </div>
        </div>
    </div>
</div>